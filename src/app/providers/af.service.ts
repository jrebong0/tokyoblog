import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
import * as firebase from 'firebase';
import { User } from './user';
import { switchMap } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AfService {
  user$: Observable<User>;

  constructor(public afAuth: AngularFireAuth,
              public afs: AngularFirestore) {

      this.user$ = this.afAuth.authState.pipe(
          switchMap(user => {
              if (user) {
                return this.afs.doc<User>(`users/${user.email}`).valueChanges();
              } else {
                return of(null)  ;
              }
          })
      );
  }

  loginWithGoogle() {
      const provider = new firebase.auth.GoogleAuthProvider();
      this.afAuth.auth.signInWithPopup(provider)
                        .then(credential =>{
                            this.updateUser(credential.user);
                        });
  }

  loginWithFacebook() {
    const provider = new firebase.auth.FacebookAuthProvider();
    this.afAuth.auth.signInWithPopup(provider)
                      .then(credential =>{
                        this.updateUser(credential.user);
                      });
  }

  updateUser(user: firebase.User) {
    const analytics = firebase.analytics();

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.email}`);

    let data: User;

    return userRef.get().subscribe( result => {
        /** if this is not an existing user, add default roles */
        if (!result.exists) {
            data = {
                uid: user.uid,
                email: user.email,
                displayName: user.displayName,
                photoURL: user.photoURL,
                roles: {
                    subscriber: true,
                    admin: false,
                }
            };
        } else {
            /** Update the user */
            data = {
                uid: user.uid,
                email: user.email,
                displayName: user.displayName,
                photoURL: user.photoURL,
                roles: {
                    subscriber: result.data().roles.subscriber,
                    admin: result.data().roles.admin,
                }
            };
        }
        userRef.set(data, {merge: true});

        analytics.logEvent('login', {
            uid: user.uid,
            email: user.email,
        });
    });
  }

  logout() {
      console.log('SIGNING OUT!!');
      this.afAuth.auth.signOut();
  }
}
