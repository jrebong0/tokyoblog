import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';

@Pipe({
  name: 'fromNow'
})
export class FromNowPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      const dt = new Date(value.seconds * 1000);
      return moment(dt).fromNow();
    }
    return null;
  }

}
