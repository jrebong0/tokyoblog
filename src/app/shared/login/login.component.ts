import { Component, OnInit, Input } from '@angular/core';
import { AfService } from '../../providers/af.service';
import { User } from '../../providers/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  user: User;
  @Input() showName: boolean;

  constructor(public afSvc: AfService) { }

  ngOnInit() {
    this.afSvc.user$.subscribe(user => {
        this.user = user;
    });
  }

  logout() {
    this.afSvc.logout();
  }

}
