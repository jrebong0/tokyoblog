import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { from } from 'rxjs';

export class Comments {
    userId: string;
    displayName: string;
    photoURL: string;
    pageId: string;
    content: string;
    create_date?: Date;
    update_date?: Date;
}

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(public afs: AngularFirestore) { }

  get(id) {
    return this.afs.collection('comments', ref => {
        return ref.where('pageId', '==', id).orderBy('update_date');
    })
    .snapshotChanges().pipe(
      map( post => {
          return post.map( a => {

              const data = a.payload.doc.data() as Comments;
              const id = a.payload.doc.id;

              return {id, ...data};
          });
      })
    );
  }

  add(comment: Comments) {
    return from(
        this.afs.collection('comments').add({...comment})
    );
  }

  delete(id: string) {
    return this.afs.doc('comments/' + id).delete();
  }
}
