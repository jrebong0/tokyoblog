import { TestBed } from '@angular/core/testing';

import { DraftsService } from './drafts.service';

describe('DraftsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DraftsService = TestBed.get(DraftsService);
    expect(service).toBeTruthy();
  });
});
