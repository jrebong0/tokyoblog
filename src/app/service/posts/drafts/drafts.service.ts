import { PostsService, Post } from './../posts.service';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class DraftsService extends PostsService {

  constructor(public afs: AngularFirestore) {
      super(afs);
      this.setTable('drafts');
  }

  getDraft(postId) {
    const drafRef: AngularFirestoreDocument<any> = this.afs.doc(`${this.table}/${postId}`);

    return drafRef.get();
  }

  saveDraft(postId, post: Post) {
    const drafRef: AngularFirestoreDocument<any> = this.afs.doc(`${this.table}/${postId}`);

    return drafRef.get().subscribe( result => {
        const uid = postId;
        const data = { uid, ...post };
        drafRef.set(data, {merge: true});
    });
  }
}
