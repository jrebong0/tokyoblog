import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { from } from 'rxjs';
import { firestore } from 'firebase';


export interface Post {
    category: string;
    title: string;
    description?: string;
    content: string;
    create_date?: firestore.Timestamp;
    update_date?: firestore.Timestamp;
    slug?: string;
    featured?: boolean;
    image?: string;
    ref?: any;
    metadesc?: string;
    metatags?: string;
    author?: string;
    views?: string;
}

@Injectable({
  providedIn: 'root'
})
export class PostsService {
    defaultTable = 'posts';
    table = 'posts';

    constructor(public afs: AngularFirestore) { }

    setTable(tableName: string) {
        this.table = tableName;
    }

    resetTableName() {
        this.table = this.defaultTable;
    }

    getPost(postId: string) {
        return this.afs.doc(`${this.table}/` + postId).snapshotChanges().pipe(
            map( post => {
                const data = post.payload.data() as Post;
                const id = post.payload.id;
                return {id, ...data};
            })
          );
    }

    getPosts(limit?: number, start?: any, startAt?: any) {
        return this.afs.collection(this.table, ref => {
            let query = ref.orderBy('update_date', 'desc');

            if (start) {
                query = query.startAfter(start);
            }

            if (startAt) {
                query = query.startAt(startAt);
            }

            if (limit) {
                query = query.limit(limit);
            }

            return query;
        })
        .snapshotChanges().pipe(
          map( post => {
              return post.map( a => {

                  const data = a.payload.doc.data() as Post;
                  const id = a.payload.doc.id;

                  /** Save the original payload for pagination purposes */
                  data.ref = a.payload.doc;

                  return {id, ...data};
              });
          })
        );
    }

    getConditionalPosts(field: string,
                        condition: any,
                        value: any,
                        limit?: number,
                        start?: any,
                        startAt?: any) {

        return this.afs.collection(this.table, ref => {
            let query = ref.where(field, condition, value).orderBy('update_date', 'desc');

            if (start) {
                query = query.startAfter(start);
            }

            if (startAt) {
                query = query.startAt(startAt);
            }

            if (limit) {
                query = query.limit(limit);
            }
            return query;
        })
        .snapshotChanges().pipe(
          map( post => {
              return post.map( a => {

                  const data = a.payload.doc.data() as Post;
                  const id = a.payload.doc.id;

                  /** Save the original payload for pagination purposes */
                  data.ref = a.payload.doc;

                  return {id, ...data};
              });
          })
        );
    }

    getFeatured(limit?: number, start?: any, startAt?: any) {
        return this.getConditionalPosts('featured', '==', true, limit, start, startAt);
    }

    getByCategory(category: string, limit?: number, start?: any, startAt?: any) {
        return this.getConditionalPosts('category', '==', category, limit, start, startAt);
    }

    getBySlug(slug) {
        return this.getConditionalPosts('slug', '==', slug, 1);
    }

    addPost(post: Post) {
        post.create_date = firestore.Timestamp.now();
        post.update_date = post.create_date;
        post.featured = post.featured || false;
        return from(
            this.afs.collection(this.table).add(post)
        );
    }

    deletePost(postId) {
        return this.afs.doc(`${this.table}/` + postId).delete();
    }

    updatePost(postId, post: Post) {
        post.update_date = firestore.Timestamp.now();
        return this.afs.doc(`${this.table}/` + postId).update(post);
    }

    /**
     * Get Image from the content
     * @param post: Content of the a post
     */
    getImage(post: Post) {
        const img = post.content.match(/<img.+src=[\'"]([^\'"]+)[\'"].*>/i);
        return img[1];
    }

    getContent(post: Post) {
        const content = post.content.replace(/<figure.+class=[\'"]([^\'"]+)[\'"]>.*<\/figure>/, '');
        return content;
    }
}
