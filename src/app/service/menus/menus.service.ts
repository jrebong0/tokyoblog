import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

export interface Menu {
    title: '';
    url: '';
}

@Injectable({
  providedIn: 'root'
})
export class MenusService {

  constructor(public afs: AngularFirestore) { }

  getMenus() {
      return this.afs.collection('menu').snapshotChanges().pipe(
        map( menu => {
            return menu.map( a => {

                const data = a.payload.doc.data() as Menu;
                const id = a.payload.doc.id;

                return {id, ...data};
            });
        })
      );
  }

  getConditionalMenus(field: string, condition: any, value: string) {
    return this.afs.collection('menu', ref => ref.where(field, condition, value)).snapshotChanges().pipe(
      map( menu => {
          return menu.map( a => {

              const data = a.payload.doc.data() as Menu;
              const id = a.payload.doc.id;

              return {id, ...data};
          });
      })
    );
}

  addMenu(menu: Menu) {
    this.afs.collection('menu').add(menu);
  }

  deleteMenu(menuId) {
      this.afs.doc('menu/' + menuId).delete();
  }

  updateMenu(menuId, menu: Menu) {
    this.afs.doc('menu/' + menuId).update(menu);
  }
}
