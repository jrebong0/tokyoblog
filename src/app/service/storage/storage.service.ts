import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { environment } from 'src/environments/environment';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private afStorage: AngularFireStorage) { }

  get(path?: string) {
    const imagePath = path || environment.basePath;
    const listRef = this.afStorage.storage.ref(imagePath);

    const firstpage = listRef.list();

    return from(firstpage);
  }
}
