import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { environment } from 'src/environments/environment';
import { finalize } from 'rxjs/internal/operators/finalize';

export class Upload {

    $key: string;
    file: File;
    name: string;
    url: string;
    progress: number;
    createdAt: Date = new Date();

    constructor(file: File) {
      this.file = file;
    }
}


@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private afStorage: AngularFireStorage) { }

  upload(file) {
    // The storage path
    const path = this.createPath(file.name);

    // create a reference to the storage bucket location
    const ref = this.afStorage.ref(path);

    // upload the file
    const task = this.afStorage.upload(path, file);

    return { ref, task };
  }

  uploadP(file) {
        // The storage path
        const path = this.createPath(file.name);

        // create a reference to the storage bucket location
        const ref = this.afStorage.ref(path);

        // // upload the file
        // this.afStorage.upload(path, file)

        return new Promise<any>((resolve, reject) => {
            const task = this.afStorage.upload(path, file);

            task.snapshotChanges().pipe(
                finalize(() => ref.getDownloadURL().subscribe(
                    res => resolve({default: res}),
                    err => reject(err))
                )
            ).subscribe();
        });
  }

  createPath(filename: string) {
    const basePath = environment.basePath;
    const dt = new Date();
    const dtPath = dt.getFullYear() + '/' + (dt.getMonth() + 1) + '/' + dt.getDate();
    return `${basePath}/${dtPath}/${filename}`;
  }
}
