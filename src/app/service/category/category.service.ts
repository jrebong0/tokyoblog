import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

export interface Category {
    title: '';
    order: '';
}

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(public afs: AngularFirestore) { }

  get() {
      return this.afs.collection('category', ref => {
        return ref.orderBy('order', 'asc');
      })
      .snapshotChanges().pipe(
        map( category => {
            return category.map( a => {

                const data = a.payload.doc.data() as Category;
                const id = a.payload.doc.id;

                return {id, ...data};
            });
        })
      );
  }

  getConditional(field: string, condition: any, value: string) {
    return this.afs.collection('category', ref => ref.where(field, condition, value)).snapshotChanges().pipe(
      map( category => {
          return category.map( a => {

              const data = a.payload.doc.data() as Category;
              const id = a.payload.doc.id;

              return {id, ...data};
          });
      })
    );
}

  add(category: Category) {
    this.afs.collection('category').add(category);
  }

  delete(categoryId) {
      this.afs.doc('category/' + categoryId).delete();
  }

  update(categoryId, category: Category) {
    this.afs.doc('category/' + categoryId).update(category);
  }
}
