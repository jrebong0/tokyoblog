import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { from } from 'rxjs';
import { take, map } from 'rxjs/operators';
import firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class PostViewsService {

  constructor(public afs: AngularFirestore) { }

  addCount(postId: string) {
    const postView = this.afs.doc('post_views/' + postId);
    return postView.valueChanges()
            .pipe(take(1))
            .subscribe( (e: any) => {
                let count = 1;
                if (e) {
                    count = e.count + 1;
                }
                postView.set({ count, postId });
            });

  }

  getCount() {
    return this.afs.collection('post_views', ref => {
        return ref.orderBy('count');
    })
    .get();
  }

  getTop(limit = 5) {
    return this.afs.collection('post_views', ref => {
        if (limit) {
            return ref.orderBy('count').limit(limit);
        } else {
            return ref.orderBy('count');
        }
    })
    .get()
    .pipe(
        map( output => {

            const pages = [];

            output.forEach( el => {
                pages.push(el.data().postId);
            });

            return this.afs.collection('posts', ref => {
                return ref.where(firebase.firestore.FieldPath.documentId(), 'in', pages);
            })
            .snapshotChanges().pipe(
                map( post => {
                    return post.map( a => {

                        const data = a.payload.doc.data();
                        const id = a.payload.doc.id;
                        let count = 0;
                        output.forEach( el => {
                            if (el.data().postId === id) {
                                count = el.data().count;
                            }
                        });

                        return {id, count, ...data};
                    });
                })
            );
        })
    );
  }
}
