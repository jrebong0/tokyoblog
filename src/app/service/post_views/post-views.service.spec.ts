import { TestBed } from '@angular/core/testing';

import { PostViewsService } from './post-views.service';

describe('PostViewsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostViewsService = TestBed.get(PostViewsService);
    expect(service).toBeTruthy();
  });
});
