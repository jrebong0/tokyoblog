import { SharedModule } from './shared.module';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { environment } from './../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule  } from '@angular/fire/storage';
import firebase from 'firebase';


import { SubscriberGuard } from './guards/subscriber.guard';
import { AdminGuard } from './guards/admin.guard';

import { AppComponent } from './app.component';
import { AfService } from './providers/af.service';
import { MenusService } from './service/menus/menus.service';
import { CategoryService } from './service/category/category.service';
import { LoginPageComponent } from './login-page/login-page.component';
import { MaterialModule } from './material.module';
import { LoginComponent } from './shared/login/login.component';

firebase.initializeApp(environment.firebase);

@NgModule({
    declarations: [
        AppComponent,
        LoginPageComponent,
        // LoginComponent
    ],
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(environment.firebase, 'tokyoblog'),
        AngularFireDatabaseModule,
        AngularFireAuthModule,
        AppRoutingModule,
        AngularFirestoreModule,
        AngularFireStorageModule,
        BrowserAnimationsModule,
        SharedModule,
        MaterialModule,
    ],
    providers: [
        AfService,
        AdminGuard,
        SubscriberGuard,
        MenusService,
        CategoryService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
