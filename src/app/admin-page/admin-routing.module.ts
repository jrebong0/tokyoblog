import { CommentsComponent } from './posts/comments/comments.component';
import { CategoryComponent } from './category/category.component';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminPageComponent } from './admin-page.component';
import { PostsComponent } from './posts/posts.component';
import { EditPostComponent } from './posts/edit-post/edit-post.component';
import { ImagesComponent } from './images/images.component';


const routes: Routes = [
  {
      path: '',
      component: AdminPageComponent,
      children: [
          { path: '', component: PostsComponent },
          { path: 'category', component: CategoryComponent },
          { path: 'dashboard', component: PostsComponent },
          { path: 'images', component: ImagesComponent },
          { path: 'posts', component: PostsComponent },
          { path: 'posts/:id', component: EditPostComponent },
          { path: 'comments/:id', component: CommentsComponent },
          { path: '**', component: PostsComponent }
      ]
  },

];


/**
 * Module responsible for setting the routes for the entire application.
 */
export const AdminRoutingModule = RouterModule.forChild(routes);
