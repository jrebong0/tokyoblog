import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  links = [
    {
        name: 'Category',
        link: '/admin/category'
    },
    {
        name: 'Posts',
        link: '/admin/posts'
    },
    {
        name: 'Images',
        link: '/admin/images'
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
