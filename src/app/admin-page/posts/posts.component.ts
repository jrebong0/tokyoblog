import { Post, PostsService } from './../../service/posts/posts.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { EditPostComponent } from './edit-post/edit-post.component';

import { Category, CategoryService } from 'src/app/service/category/category.service';
import { PostViewsService } from 'src/app/service/post_views/post-views.service';

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css']
  })
export class PostsComponent implements OnInit {
  postDetails: Post = {
      category: '',
      title: '',
      content: ''
  };

  dataSource = new MatTableDataSource();
  displayedColumns = ['title', 'category', 'date', 'views', 'actions'];
  categories: Category[];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private postSvc: PostsService,
              private categorySvc: CategoryService,
              public dialog: MatDialog,
              private postViewSvc: PostViewsService,
              )
              {

  }

  ngOnInit() {
      this.postSvc.getPosts().subscribe(data => {

        this.postViewSvc.getCount().subscribe( e => {

            let views = [];
            e.forEach(el => {
                let countData = el.data();
                views [countData.postId] = countData.count;
            });
            console.log(views);

            let dataWithView = data.map(e => {
                e.views = views[e.id];
                return e;
            });
            this.dataSource.data = dataWithView;
        });
      });

      this.categorySvc.get().subscribe(categories => this.categories = categories);
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  addPost() {

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  editPost(id: string, data: Post) {
    this.postSvc.updatePost(id, data);
  }

  deletePost(id: string) {
    this.postSvc.deletePost(id);
  }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '250px',
        height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === 'true') {
          this.deletePost(id);
      }
    });
  }

  openEditDialog(id: string, title: string, content: string, category: string) {
    const dialogRef = this.dialog.open(EditPostComponent, {
        width: '250px',
        height: 'auto',
        data: { title, content, category, categories: this.categories }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'false') {
        this.editPost(id, { title: result.title, content: result.content, category: result.category });
      }
    });
  }

}
