import { DraftsService } from './../../../service/posts/drafts/drafts.service';
import { UploadService } from './../../../service/upload/upload.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService, Category } from 'src/app/service/category/category.service';
import { UploadAdapter } from '../../shared/uploader/adapter/upload-adapter';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ImagesComponent } from '../../images/images.component';
import { PostsService, Post } from '../../../service/posts/posts.service';
import { User } from '../../../providers/user';
import { AfService } from '../../../providers/af.service';

// import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import * as ClassicEditor from '../../../../ckeditor/ckeditor';

interface CustomWindow extends Window {
    editor: any;
}

declare let window: CustomWindow;


@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

  user: User;
  postForm: FormGroup;
  postId: string;
  categories: Category[];
//   ckEditor = ClassicEditor;
//   ckEditor = CustomEditor;
  ckConfig = {
    // extraPlugins: [ this.MyCustomUploadAdapterPlugin ],
    toolbar: {
        items: [
            'heading',
            '|',
            'bold',
            'italic',
            'link',
            'insertTable',
            'bulletedList',
            'numberedList',
            '|',
            'alignment',
            'fontBackgroundColor',
            'fontColor',
            'fontSize',
            'fontFamily',
            'highlight',
            '|',
            'code',
            'codeBlock',
            'blockQuote',
            '|',
            'strikethrough',
            'subscript',
            'superscript',
            'underline',
            '|',
            'indent',
            'outdent',
            'imageUpload',
            'mediaEmbed',
            'undo',
            'redo',
            '|',
            'horizontalLine',
            'pageBreak',
            'removeFormat',
            'todoList'
        ]
    },
    language: 'en',
    image: {
        toolbar: [
            'imageTextAlternative',
            'imageStyle:full',
            'imageStyle:side'
        ]
    },
    table: {
        contentToolbar: [
            'tableColumn',
            'tableRow',
            'mergeTableCells'
        ]
    },
  };
  dialogRef: MatDialogRef<ImagesComponent, any>;
  hasDraft = false;

  ckEditor = ClassicEditor;


  constructor(private fb: FormBuilder,
              public dialog: MatDialog,
              private afSvc: AfService,
              private postSvc: PostsService,
              private draftSvc: DraftsService,
              private route: ActivatedRoute,
              private router: Router,
              private uploadSvc: UploadService,
              private categorySvc: CategoryService) {

    this.postForm = this.fb.group({
        title: ['', Validators.required ],
        slug: ['', Validators.required ],
        category: ['', Validators.required ],
        image: '',
        content: ['', Validators.required ],
        featured: ['', Validators.required ],
        description: ['', Validators.required ],
        metadesc: '',
        metatags: '',
      });
  }

  MyCustomUploadAdapterPlugin( editor ) {
      editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
          const adapter = new UploadAdapter(loader);
          adapter.uploadSvc = this.uploadSvc;
          return new UploadAdapter(loader);
      };
  }

  ngOnInit() {
    this.categorySvc.get().subscribe(categories => this.categories = categories);

    this.afSvc.user$.subscribe(user => {
        this.user = user;
    });


    this.route.params.subscribe ( params => {
        if (params.id === '0') {
            return;
        }

        this.postId = params.id;
        /** test first if there is a draft */
        this.draftSvc.getPost(this.postId).subscribe(draft => {
            /** has draft - get the data */
            if(draft.content) {
                this.hasDraft = true;
                this.setFormValue(draft);
            } else {
                this.hasDraft = false;
                this.postSvc.getPost(this.postId).subscribe(post => this.setFormValue(post));
            }
        });
    });
  }

  setFormValue(post: Post) {
    this.postForm.setValue({
        title: post.title,
        slug: post.slug || '',
        category: post.category,
        image: post.image,
        content: post.content,
        featured: post.featured,
        description: post.description || '',
        metadesc: post.metadesc || '',
        metatags: post.metatags || '',
    });
  }

  onCkReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
    );

    editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
        const adapter = new UploadAdapter(loader);
        adapter.uploadSvc = this.uploadSvc;
        return adapter;
    };
  }

  createSlug() {
    if (!this.postForm.value.slug) {
        this.postForm.patchValue({
            slug: this.postForm.value.title.toLowerCase().replace(/\W+/g, '-')
        });
    }
  }

  addPost() {
      const post: Post = this.postForm.value;
      post.author = this.user.displayName;

      this.postSvc.addPost(this.postForm.value).subscribe( docRef  => {
          this.postId = docRef.id;

          this.router.navigate(['/admin/posts', docRef.id]);
      });
  }

  editPost() {
      const post: Post = this.postForm.value;
      post.author = this.user.displayName;
      this.postSvc.updatePost(this.postId, this.postForm.value);

      /** Delete the draft as it is no longer needed */
      if (this.hasDraft) {
        this.draftSvc.deletePost(this.postId);
        this.hasDraft = false;
      }
  }

  saveDraft() {
    this.draftSvc.saveDraft(this.postId, this.postForm.value);
  }

  openImageDialog() {
    const self = this;
    self.dialogRef = this.dialog.open(ImagesComponent, {
        width: '980px',
        height: 'auto',
        minHeight: '400px',
        data: {  }
    });

    self.dialogRef.componentInstance.selectedImage.subscribe(url => {
        self.dialogRef.close();
        self.postForm.patchValue({ image: url});
    });
  }


}
