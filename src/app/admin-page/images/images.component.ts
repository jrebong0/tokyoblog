import { Observable } from 'rxjs';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UploaderComponent } from '../shared/uploader/uploader.component';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {

  @Output() selectedImage =  new EventEmitter();

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const self = this;
    const dialogRef = this.dialog.open(UploaderComponent, {
        width: '500px',
        height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {
        dialogRef.componentInstance.downloadURL.subscribe(url => {
            self.selectedImage.emit(url);
        });
    });
  }

  selectImage(url) {
    this.selectedImage.emit(url);
  }


}
