import { AfService } from './../../providers/af.service';
import { User } from './../../providers/user';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user: User;

  @Output() public toggle = new EventEmitter();

  constructor(public afSvc: AfService) { }

  ngOnInit() {
    this.afSvc.user$.subscribe(user => {
        this.user = user;
    });
  }

  toggler() {
    this.toggle.emit(true);
  }

}
