import { StorageService } from './../../../service/storage/storage.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.css']
})
export class ImageListComponent implements OnInit {
  folders;
  images;
  parentFolder;

  @Output() selectedImage =  new EventEmitter();

  constructor(private storageSvc: StorageService) { }

  ngOnInit() {
      this.openFolder();
  }

  openFolder(path?: string) {

    this.storageSvc.get(path).subscribe( e => {
        const self = this;
        this.images = [];
        this.folders = e.prefixes;

        if (e.prefixes && e.prefixes.length > 0 && e.prefixes[0].parent) {
            this.parentFolder = e.prefixes[0].parent.parent;
        }

        e.items.forEach(element => {
            element.getMetadata().then(fileData => {
                const imageMetaData = fileData;

                element.getDownloadURL().then(fileUrl => {
                    imageMetaData.downloadUrl = fileUrl;
                    self.images.push(imageMetaData);
                });
            });
        });
      });
  }

  selectImage(url) {
    this.selectedImage.emit(url);
  }

}
