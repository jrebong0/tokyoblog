import { UploadService } from 'src/app/service/upload/upload.service';
import { finalize } from 'rxjs/internal/operators/finalize';

export class UploadAdapter {
    loader: any;
    public uploadSvc: UploadService;

    constructor(loader) {
        // The file loader instance to use during the upload.
        this.loader = loader;
    }

    // Starts the upload process.
    upload() {
        const self = this;
        return this.loader.file.then( file => self.uploadSvc.uploadP(file) );
    }

    // Aborts the upload process.
    abort() {
    }
}
