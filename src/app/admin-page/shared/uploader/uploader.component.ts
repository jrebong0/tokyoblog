import { Component, OnInit } from '@angular/core';
import { UploadService } from 'src/app/service/upload/upload.service';
import { finalize } from 'rxjs/internal/operators/finalize';
import { AngularFireUploadTask } from '@angular/fire/storage';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.css']
})
export class UploaderComponent implements OnInit {
    // Main task
  task: AngularFireUploadTask;

  // Progress monitoring
  percentage: Observable<number>;
  snapshot: Observable<any>;

  // Download URL
  downloadURL: Observable<string>;

  // State for dropzone CSS toggling
  isHovering: boolean;

  uploadProgress;

  constructor(private uploadSvc: UploadService) { }

  ngOnInit() {
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  upload(event: FileList) {
      const self = this;

      // The File object
      const file = event.item(0);

      // Client-side validation example
      if (file.type.split('/')[0] !== 'image') {
        console.error('unsupported file type :( ')
        return;
      }

      const upload = this.uploadSvc.upload(file);
      this.task = upload.task;

      this.percentage = this.task.percentageChanges();
      this.snapshot   = this.task.snapshotChanges();

      this.task.snapshotChanges().pipe(
            finalize( () => {
                self.downloadURL = upload.ref.getDownloadURL();
            })
        )
        .subscribe();
  }

  // Determines if the upload task is active
  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }

}
