import { Post } from './../../service/posts/posts.service';
import { MenusService, Menu } from './../../service/menus/menus.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menuDetails: Menu = {
      title: '',
      url: ''
  };
  dataSource = new MatTableDataSource();
  displayedColumns = ['id', 'title', 'url', 'actions'];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private menuSvc: MenusService,
              public dialog: MatDialog) { }

  ngOnInit() {
      this.menuSvc.getMenus().subscribe(data => {
        this.dataSource.data = data;
      });
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  addMenu() {
      this.menuSvc.addMenu(this.menuDetails);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  editMenu(id: string, data: Menu) {
    this.menuSvc.updateMenu(id, data);
  }

  deleteMenu(id: string) {
    this.menuSvc.deleteMenu(id);
  }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '250px',
        height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === 'true') {
          this.deleteMenu(id);
      }
    });
  }

  openEditDialog(id: string, title: string, url: string) {
    const dialogRef = this.dialog.open(EditMenuComponent, {
        width: '250px',
        height: 'auto',
        data: { title, url }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'false') {
        this.editMenu(id, result);
      }
    });
  }

}
