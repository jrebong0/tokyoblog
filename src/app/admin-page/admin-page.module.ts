import { SharedModule } from './../shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminPageComponent } from './admin-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MenuComponent } from './menu/menu.component';
import { PostsComponent } from './posts/posts.component';
import { FormsModule } from '@angular/forms';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';
import { EditPostComponent } from './posts/edit-post/edit-post.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CategoryComponent } from './category/category.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { UploaderComponent } from './shared/uploader/uploader.component';
import { ImagesComponent } from './images/images.component';
import { DropzoneDirective } from './shared/uploader/dropzone/dropzone.directive';
import { ImageListComponent } from './shared/image-list/image-list.component';
import { CommentsComponent } from './posts/comments/comments.component';
import { PreviousComponent } from './shared/previous/previous.component';
import { EditMenuComponent } from './menu/edit-menu/edit-menu.component';


@NgModule({
  declarations: [
    DashboardComponent,
    AdminPageComponent,
    NavbarComponent,
    MenuComponent,
    PostsComponent,
    ConfirmationDialogComponent,
    EditPostComponent,
    CategoryComponent,
    EditCategoryComponent,
    UploaderComponent,
    ImagesComponent,
    DropzoneDirective,
    ImageListComponent,
    CommentsComponent,
    PreviousComponent,
    EditMenuComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    FlexLayoutModule,
    SharedModule
  ],
  entryComponents: [ConfirmationDialogComponent, EditCategoryComponent, UploaderComponent]
})
export class AdminPageModule { }
