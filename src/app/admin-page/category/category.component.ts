import { Category, CategoryService } from './../../service/category/category.service';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { EditCategoryComponent } from './edit-category/edit-category.component';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categoryDetails: Category = {
      title: '',
      order: ''
  };
  dataSource = new MatTableDataSource();
  displayedColumns = ['title', 'order', 'actions'];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private categorySvc: CategoryService,
              public dialog: MatDialog) { }

  ngOnInit() {
      this.categorySvc.get().subscribe(data => {
        this.dataSource.data = data;
      });
  }

  // tslint:disable-next-line: use-lifecycle-interface
  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  add() {
      this.categorySvc.add(this.categoryDetails);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim().toLowerCase();
    this.dataSource.filter = filterValue;
  }

  edit(id: string, data: Category) {
    this.categorySvc.update(id, data);
  }

  delete(id: string) {
    this.categorySvc.delete(id);
  }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '250px',
        height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result === 'true') {
          this.delete(id);
      }
    });
  }

  openEditDialog(id: string, title: string, order: string) {
    const dialogRef = this.dialog.open(EditCategoryComponent, {
        width: '250px',
        height: 'auto',
        data: { title, order }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== 'false') {
        this.edit(id, result);
      }
    });
  }

}
