import { NgModule } from '@angular/core';
import { MatButtonModule, MatToolbarModule, MatSortModule, MatPaginatorModule, MatTooltipModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
    declarations: [

    ],
    imports: [
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatMenuModule,
      MatSidenavModule,
      MatListModule,
      MatTableModule,
      MatFormFieldModule,
      MatInputModule,
      MatSortModule,
      MatPaginatorModule,
      MatDialogModule,
      MatSelectModule,
      MatCardModule,
      MatCheckboxModule,
      MatTooltipModule
    ],
    exports: [
      MatButtonModule,
      MatToolbarModule,
      MatIconModule,
      MatMenuModule,
      MatSidenavModule,
      MatListModule,
      MatTableModule,
      MatFormFieldModule,
      MatInputModule,
      MatSortModule,
      MatPaginatorModule,
      MatDialogModule,
      MatSelectModule,
      MatCardModule,
      MatCheckboxModule,
      MatTooltipModule
    ]
  })
  export class MaterialModule { }
