import { LoginPageComponent } from './login-page/login-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';
import { SubscriberGuard } from './guards/subscriber.guard';

const routes: Routes = [
    { path: 'admin', loadChildren: 'src/app/admin-page/admin-page.module#AdminPageModule', canActivate: [AdminGuard] },
    { path: 'login', component: LoginPageComponent, },
    { path: 'index', loadChildren: 'src/app/front-page/front-page.module#FrontPageModule' },
    { path: '**', redirectTo: 'index'},
];


/**
 * Module responsible for setting the routes for the entire application.
 */
@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [RouterModule],
    providers: []
})
export class AppRoutingModule { }
