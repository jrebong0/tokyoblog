import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService, Post } from '../../service/posts/posts.service';
import { ActivatedRoute } from '@angular/router';
import firebase from 'firebase';

@Component({
  selector: 'app-pages-list',
  templateUrl: './pages-list.component.html',
  styleUrls: ['./pages-list.component.css']
})
export class PagesListComponent implements OnInit {
  posts: Post[];
  category: string;
  limit = 10;

  startRef: any;
  prevRef = [];
  nextRef: any;


  constructor(private postsSvc: PostsService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe ( params => {
        const analytics = firebase.analytics();

        analytics.logEvent('page_view', {
          page_path: `/index/${params.category}`,
          page_title: 'home'
        });

        this.category = params.category;
        this.getPost(this.category);
    });
  }

  previous() {
    this.getPost(this.category, null, this.prevRef.pop());
  }

  next() {
    this.prevRef.push(this.posts[0].ref);
    this.getPost(this.category, this.nextRef);
  }

  getPost(category, start?: any, end?: any) {
    if (category) {
        this.postsSvc.getByCategory(category, this.limit, start, end).subscribe( posts => this.storeRef(posts));
    } else {
        this.postsSvc.getPosts(this.limit, start, end).subscribe( posts => this.storeRef(posts));
    }
  }

  storeRef(posts) {
    this.posts = posts;
    const len = this.posts.length - 1;
    this.nextRef  = this.posts[len].ref;
  }
}
