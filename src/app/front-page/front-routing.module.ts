import { PagesComponent } from './pages/pages.component';
import { Routes, RouterModule } from '@angular/router';
import { PagesListComponent } from './pages-list/pages-list.component';
import { FrontPageComponent } from './front-page.component';
import { HomePageComponent } from './home-page/home-page.component';



const routes: Routes = [
  {
      path: '',
      component: FrontPageComponent,
      children: [
          { path: '', component: HomePageComponent },
          { path: 'home', component: HomePageComponent },
          { path: ':category', component: PagesListComponent },
          { path: ':category/:slug', component: PagesComponent },
          { path: '**', redirectTo: 'home' }
      ]
  },

];


/**
 * Module responsible for setting the routes for the entire application.
 */
export const FrontRoutingModule = RouterModule.forChild(routes);
