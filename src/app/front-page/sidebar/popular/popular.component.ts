import { Component, OnInit } from '@angular/core';
import { Post } from '../../../service/posts/posts.service';
import { PostViewsService } from '../../../service/post_views/post-views.service';


@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent implements OnInit {
  posts;

  constructor(private postViewSvc: PostViewsService) { }

  ngOnInit() {
      this.postViewSvc.getTop(10).subscribe( e => {
        e.subscribe( (o: any[]) => {
            this.posts = o.sort( (a, b) => b.count - a.count );
        });
      });
  }

}
