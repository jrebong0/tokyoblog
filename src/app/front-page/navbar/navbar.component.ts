import { Category, CategoryService } from './../../service/category/category.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { AfService } from '../../providers/af.service';
import { User } from '../../providers/user';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user: User;
  categoryList: Category[];

  @Output() public sidenavToggle = new EventEmitter();

  constructor(public afSvc: AfService,
              private categorySvc: CategoryService) { }

  ngOnInit() {
    this.afSvc.user$.subscribe(user => {
        this.user = user;
    });

    this.categorySvc.get().subscribe( categoryList => this.categoryList = categoryList );
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }

  logout() {
      this.afSvc.logout();
  }

}
