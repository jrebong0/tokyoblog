import { SharedModule } from './../shared.module';
import { MaterialModule } from './../material.module';
import { FrontRoutingModule } from './front-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesListComponent } from './pages-list/pages-list.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomePageComponent } from './home-page/home-page.component';
import { FrontPageComponent } from './front-page.component';
import { PagesComponent } from './pages/pages.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AboutComponent } from './sidebar/about/about.component';
import { LatestComponent } from './sidebar/latest/latest.component';
import { FooterComponent } from './footer/footer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HeaderComponent } from './header/header.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FromNowPipe } from '../providers/pipes/from-now.pipe';
import { PopularComponent } from './sidebar/popular/popular.component';

@NgModule({
  declarations: [
    PagesListComponent,
    NavbarComponent,
    HomePageComponent,
    FrontPageComponent,
    PagesComponent,
    SidebarComponent,
    AboutComponent,
    LatestComponent,
    FooterComponent,
    HeaderComponent,
    CommentsComponent,
    FromNowPipe,
    PopularComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FrontRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    SharedModule
  ],
  entryComponents: []
})
export class FrontPageModule { }
