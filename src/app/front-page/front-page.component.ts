import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CategoryService, Category } from '../service/category/category.service';
import { MatIconRegistry } from "@angular/material";


@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FrontPageComponent implements OnInit {
  categoryList: Category[];

  constructor(private categorySvc: CategoryService,
              public matIconRegistry: MatIconRegistry) {
                matIconRegistry.registerFontClassAlias('fontawesome', 'fa');
              }

  ngOnInit() {
    this.categorySvc.get().subscribe( categoryList => this.categoryList = categoryList );
  }

}
