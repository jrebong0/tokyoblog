import { Component, OnInit } from '@angular/core';
import { PostsService, Post } from '../../service/posts/posts.service';
import firebase from 'firebase';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  posts: Post[];
  bigFeature: Post;
  smallFeature: Post[];

  constructor(private postsSvc: PostsService) {
    const analytics = firebase.analytics();

    analytics.logEvent('page_view', {
      page_path: '/index',
      page_title: 'home'
    });
  }

  ngOnInit() {
    // this.postsSvc.getFeatured(3).subscribe( posts => {
    //     this.bigFeature = posts[0];
    //     this.smallFeature = posts.slice(1, 3);
    // });

    // this.postsSvc.getPosts().subscribe( posts => this.posts = posts );
  }
}
