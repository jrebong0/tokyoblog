import { CommentsService, Comments } from './../../../service/comments/comments.service';
import { AfService } from './../../../providers/af.service';
import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { User } from '../../../providers/user';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { FromNowPipe } from '../../../providers/pipes/from-now.pipe';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
})
export class CommentsComponent implements OnInit, OnChanges {
  user: User;
  comments: Comments[] = [];
  postForm: FormGroup;

  @Input() pageId: string;

  constructor(public afSvc: AfService,
              private fb: FormBuilder,
              public commentSvc: CommentsService) {

        this.postForm = this.fb.group({
            comment: ['', Validators.required ],
        });
    }

  ngOnInit() {

    this.afSvc.user$.subscribe(user => {
        this.user = user;
    });
  }

  ngOnChanges() {
    if (this.pageId) {
        this.commentSvc.get(this.pageId).subscribe(comments => {
            this.comments = comments;
        });
    }
  }

  addComment() {
    const comment = new Comments();
    comment.pageId = this.pageId;
    comment.displayName = this.user.displayName;
    comment.photoURL = this.user.photoURL;
    comment.create_date = new Date();
    comment.update_date = comment.create_date;
    comment.userId = this.user.uid;
    comment.content = this.postForm.value.comment;
    this.commentSvc.add(comment).subscribe( e => {
        this.postForm.reset();
    });
  }

  loginFb() {

  }

  loginGoogle() {

  }

}
