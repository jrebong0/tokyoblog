import { environment } from './../../../environments/environment.prod';
import { Category } from './../../service/category/category.service';
import { PostsService, Post } from './../../service/posts/posts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import firebase from 'firebase/app';
import { PostViewsService } from '../../service/post_views/post-views.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {
  category: Category;
  post: Post;
  data: any;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private meta: Meta,
              private postViewSvc: PostViewsService,
              private titleService: Title,
              private postsSvc: PostsService) {

      this.route.params.subscribe ( params => {

          const newSlug = params.slug.replace(/\.[^/.]+$/, '');

          this.postsSvc.getBySlug(newSlug).subscribe(post => {
              if (post[0]) {
                const analytics = firebase.analytics();

                this.post = post[0];
                this.data = this.post;
                this.titleService.setTitle( this.post.title);

                const path = `index/${params.category}/${this.post.slug}`;
                this.setMeta(this.post, path);


                analytics.logEvent('page_view', {
                    page_path: path,
                    page_title: this.post.title
                });
                this.postViewSvc.addCount(this.data.id);
              }
          });
      });
  }

  ngOnInit() {

  }

  setMeta(post: Post, url: string) {

    this.meta.updateTag({ name: 'description', content: post.metadesc });
    this.meta.updateTag({ name: 'keywords', content: post.metatags });

    this.meta.updateTag({ property: 'og:url', content: 'https://tokyomanila.com/' + url + '.html' });
    this.meta.updateTag({ property: 'og:title', content: post.title });
    this.meta.updateTag({ property: 'og:description', content: post.metadesc });

    this.meta.updateTag({ name: 'twitter:image', content: post.image });
    this.meta.updateTag({ property: 'og:image', content: post.image });
  }

}
