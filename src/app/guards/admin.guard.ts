import { AfService } from './../providers/af.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
    constructor(private af: AfService) {}

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.af.user$.pipe(
            take(1),
            map(user => user && user.roles.admin ? true : false),
            tap( isAdmin => {
                if (!isAdmin) {
                    console.log('Not An Admin');
                }
            })
        );
    }

}
