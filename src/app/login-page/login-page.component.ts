import { AfService } from './../providers/af.service';
import { User } from '../providers/user';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  user: User;

  constructor(public afSvc: AfService) { }

  ngOnInit() {
    this.afSvc.user$.subscribe(user => this.user = user);
  }

  login() {
      this.afSvc.loginWithGoogle();
  }

}
