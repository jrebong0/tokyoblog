import { LoginComponent } from './shared/login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        CommonModule,
        MaterialModule
    ],
    exports: [
        LoginComponent
    ]
  })
  export class SharedModule { }
